<div class="row">
	<div class="col-md-3 col-md-offset-4">
    <div class="login-panel panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $modal_title ?></h3>
      </div>
      <div class="panel-body">
        <?php echo validation_errors(); ?>

        <?php echo form_open('', array('id'=>'user-login', 'class'=>'user-login', 'role'=>"form")); ?>
          <fieldset>
            <div class="form-group">
              <input class="form-control" placeholder="User Name" name="usr_alias" type="text" autofocus>
            </div>
            <div class="form-group">
              <input class="form-control" placeholder="Password" name="usr_password" type="password" value="">
            </div>
            <div class="checkbox">
              <label>
                <input name="remember" type="checkbox" value="Remember Me">Remember Me
              </label>
            </div>
            <!-- Change this to a button or input when using this as a form -->
            <button type="submit" class="btn btn-lg btn-primary btn-block">Login</button>
          </fieldset>
        <?php echo form_close(); ?>
      </div>
    </div>
	</div>
</div>
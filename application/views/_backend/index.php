<?php
	// Main views template handler 
	$this->load->view('_backend/header'); ?>

<?php if( !is_null($navbar_menu) ): ?>
	<div id="page-header" class="row">
		<div class="col-lg-12">
			<h2 class="page-header"><?php echo $page_header ?></h2>
		</div>
	</div>
<?php endif; ?>

<?php
  // Check $subview value(s)
  if (!is_null($subview)) {
    foreach ($subview as $view) // Loop through $subview
      $this->load->view($view); // Load and Display view(s) template 
  }
?>

<?php $this->load->view('_backend/footer'); ?>          
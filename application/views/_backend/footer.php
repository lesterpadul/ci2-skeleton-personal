
      </div>
      <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <?php
      // Load JavaScript/jQuery files 
      if (!is_null($footer_scripts)): ?>
      <?php
        // Loop through $footer_scripts variable 
        foreach ($footer_scripts as $key => $script): ?> 
        <script src="<?php echo base_url( $script ) ?>"></script>
      <?php endforeach; ?>
    <?php endif; ?>
  </body>
</html>
<div class="row">
  <div class="col-md-5">
    <?php echo form_open_multipart(base_url('sample/upload/process'), array('id'=>'upload', 'class'=>'upload', 'role'=>"form")); ?>
      <!-- <div class="form-group">
        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
          <div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
          <span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Select file</span><span class="fileinput-exists">Change</span><input type="file" name="myfile"></span>
          <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
        <p class="help-block small"><strong>Note: </strong>Allowed are *.jpg, *.png and *.gif</p>
      </div> -->

      <!-- Default Bootstrap Input File field -->
      <div class="form-group">
        <label for="file">File input</label>
        <!-- <input type="file" id="myfile" name="myfile"> -->
        <input type="file" name="myfile[]" multiple>
        <p class="help-block">Example block-level help text here.</p>
      </div>
      
      <button type="submit" class="btn btn-default">Upload</button>
    <?php echo form_close(); ?>
  </div>
</div>
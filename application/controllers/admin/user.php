<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class User extends Admin_Controller {
		public function __construct(){
	    parent::__construct();
	  }
	      
		public function index() {
			$this->load->view('welcome_message');
		}

		public function login() {
		  
		  $this->data['navbar_menu'] = NULL; // Disabled Navigation Menus

		  $this->data['modal_title'] = '<i class="fa fa-user"></i> Please Sign-In';

		  $dashboard = 'dashboard';

		  $this->m_tbl_users->loggedin() == FALSE || redirect($dashboard);

		  $rules = $this->m_tbl_users->user_login;
		  $this->form_validation->set_rules($rules);
		  
		  if ($this->form_validation->run() == TRUE) {
		    if ($this->m_tbl_users->login() == TRUE) {
		      redirect($dashboard);
		    } else {
		      $this->session->set_flashdata('Error', 'Email or Password Combination Invalid!');
		      redirect('admin/user/login');
		    }
		  }

			$this->data['subview'][] = '_backend/user/login';
			$this->load->view('_backend/index', $this->data);
		}

    public function logout() {
	    $this->m_tbl_users->logout();
	    redirect('admin/user/login');
	  }
    
		// End User Class
	}
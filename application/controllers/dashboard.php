<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Dashboard extends Admin_Controller {
		public function __construct() {
      parent::__construct();
    }
    
		public function index() {
			$this->data['page_header'] = 'Dashboard';
			$this->data['subview'][] = '_backend/user/dashboard';
			
			$this->load->view('_backend/index', $this->data);
		}

		// End Dashboard Class
	}
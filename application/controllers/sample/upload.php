<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Upload extends Admin_Controller {
		public function __construct(){
	    parent::__construct();
	    $this->load->library('upload');
	  }
	      
		public function index() {
			$this->data['page_header']  = 'File Upload Sample';
			$this->data['subview'][]    = '_sample/upload/file';
			$this->load->view('_sample/index', $this->data);
		}

		public function process() {
      /**
       * Config your upload settings
       * Ref: http://www.codeigniter.com/user_guide/libraries/file_uploading.html
       */
			$config = array(
				'upload_path'   => 'demo', // No slash(/) both end
				'allowed_types' => 'gif|jpg|png',
			);

			$input_field_name = 'myfile'; // Input File field

			// Note: Load first the "$this->load->library('upload');" 
			$data = $this->process_upload($input_field_name, $config);

			dump_exit($data);
		}

		// End Upload Class
	}